# -*- coding: utf-8 -*-

# This code is part of qcolib.
#
# (C) Copyright Kevin Pack, Miralem Sinanovic 2020
#
# This code is licensed under the Apache License, Version 2.0. You may
# obtain a copy of this license in the LICENSE.txt file in the root directory
# of this source tree or at http://www.apache.org/licenses/LICENSE-2.0.
#
# Any modifications or derivative works of this code must retain this
# copyright notice, and modified files need to carry a notice indicating
# that they have been altered from the originals.


import os
import pickle
import pprint
import numpy as np


class Convergence(Exception): pass

class ATC:
    """
        ASK-TELL COUNTER
    """
    def __init__(self):
        self.cntr = 0


class Archive:
    """
    Archive of the optimization process
    arc = { 1 : {x   : [x],          # [x] values given by the algorithm to test
                 f_x : [f_x],        # function values of [x]
                 hparams: {hparams}}   # hyper parameters of the optimizer
           }
    """
    def __init__(
        self
    ):
        self.hist = {}


        self.export_setup_exec = False
        self.import_setup_exec = False



    def print_history(self, hist = {}, hist_range = []):
        if hist == {}:
            hist = self.hist

        if hist_range == []:
            pprint.pprint(hist)
        else:
            tmp = self.get_history_range(hist, hist_range)
            pprint.pprint(tmp)



    def add_to_history(self, atc, x, f_x):
        #if len(x) != len(f_x):
        #    raise Exception("In tell(): len of list of x != len of list of f_x!")

        tmp = {"x"   : x,
               "f_x" : f_x}
        self.hist[atc] = tmp


    def get_history_range(self, hist = {}, hist_range = []):
        if hist == {}:
            hist = self.hist

        if hist_range == []:
            tmp = hist
        else:
            tmp = {}
            for pos in hist_range:
                if pos not in list(hist.keys()):
                    raise Exception("Position not in history!")
                tmp[pos] = hist[pos]
        return tmp


    def export_pickle(self, hist = {}, hist_range = []):
        if hist == {}:
            hist = self.hist

        if hist_range == []:
            tmp = hist
        else:
            tmp = self.get_history_range(hist, hist_range)

        with open(self.export_full_path, 'wb') as handle:
            pickle.dump(tmp, handle, protocol=pickle.HIGHEST_PROTOCOL)


    def import_pickle(self, hist_range = []):
        with open(self.import_full_path, 'rb') as handle:
            tmp = pickle.load(handle)

        tmp = self.get_history_range(tmp, hist_range)

        return tmp


    # TODO: just copied this hacked function here to not loose it. still needs to be rewritten.
#    def export_plain(self, hist = {}):
#         with open('qcolib.dat', 'a+') as file:
            # for j in range(0, int(popsize)):
                # for i in range(0, dim):
                    # file.write(str(x[j][i]) + "\t")
                # file.write(str(f_x[j]) + "\n")
            # file.write("\n")


    def export_setup(self, dir, name, ftype):
        self.export_dir = os.path.abspath(dir)
        self.export_name = name
        self.export_ftype = ftype

        full_path = self.export_dir + "/" + self.export_name + "." + self.export_ftype
        self.export_full_path = os.path.abspath(full_path)

        # check if export_dir exists
        if not os.path.exists(self.export_dir):
            # TODO: exchange os.mkdir for os.makedirs(...). os.makedirs <=> mkdir -p
            os.mkdir(self.export_dir)

        # TODO: think if "touch" of file should be implement and if so how
#         if not os.path.exists(self.export_full_path):
            # with open(self.export_full_path, "w") as file:
                # file.write("")

        self.export_setup_exec = True


    def import_setup(self, dir, name, ftype):
        self.import_dir = os.path.abspath(dir)
        self.import_name = name
        self.import_ftype = ftype

        full_path = self.import_dir + "/" + self.import_name + "." + self.import_ftype
        self.import_full_path = os.path.abspath(full_path)

        self.import_setup_exec = True


    def export_history(self, hist = {}, hist_range = []):
        if self.export_setup_exec == False:
            raise Exception("Export variables not set! Execute export_setup first!")

        if hist == {}:
            hist = self.hist

        tmp = self.get_history_range(hist = hist, hist_range=hist_range)

        if self.export_ftype == "pickle":
            self.export_pickle(tmp)


    def import_history(self, hist_range = []):
        if self.export_setup_exec == False and self.import_setup_exec == False:
            raise Exception("Cannot use default settings. Please execute export_setup or import_setup first!")

        if self.import_setup_exec == False:
            self.import_setup(self.export_dir, self.export_name, self.export_ftype)

        if self.import_ftype == "pickle":
            tmp = self.import_pickle(hist_range)

        return tmp


    def get_global_f_x_pos(self, hist = {}, hist_range = []):
        cur_min = None
        cur_pos = None

        if hist == {}:
            hist = self.hist

        tmp = self.get_history_range(hist = hist, hist_range=hist_range)

        for atc_cntr in tmp.keys():
            f_x = (tmp[atc_cntr])["f_x"]
            for i in range(len(f_x)):
                if cur_min == None:
                    cur_min = f_x[i]
                    cur_pos = (atc_cntr, i)
                else:
                    if f_x[i] < cur_min:
                        cur_min = f_x[i]
                        cur_pos = (atc_cntr, i)
        return cur_pos


    def get_local_f_x_pos(self, hist = {}, hist_range = []):
        if hist == {}:
            hist = self.hist

        tmp = self.get_history_range(hist = hist, hist_range=hist_range)

        pos_list = []
        for atc_cntr in tmp.keys():
            cur_min = None
            cur_pos = None
            f_x = (tmp[atc_cntr])["f_x"]
            for i in range(len(f_x)):
                if cur_min == None:
                    cur_min = f_x[i]
                    cur_pos = (atc_cntr, i)
                else:
                    if f_x[i] < cur_min:
                        cur_min = f_x[i]
                        cur_pos = (atc_cntr, i)
            pos_list.append(cur_pos)
        return pos_list


    def get_element(self, pos):
        atc_cntr = pos[0]
        batch_pos = pos[1]
        ele = self.hist[atc_cntr]
        if batch_pos == None:
            return ele
        else:
            tmp = {}
            tmp["atc"] = atc_cntr
            tmp["x"] = (ele["x"])[batch_pos]
            tmp["f_x"] = (ele["f_x"])[batch_pos]
            return tmp


    def get_f_x_range(self, hist = [], hist_range = []):
        if hist == []:
            hist = self.hist

        tmp = self.get_history_range(hist = hist, hist_range = hist_range)

        f_x = []
        for pos in tmp:
            f_x.append(tmp[pos]["f_x"])

        return np.asarray(f_x)


    def get_x_range(self, hist = [], hist_range = []):
        if hist == []:
            hist = self.hist
        tmp = self.get_history_range(hist = hist, hist_range = hist_range)

        x = []
        for pos in tmp:
            x.append(tmp[pos]["x"])

        return np.asarray(x)





# NOTE: taken from scipy. this is the python implementation of brent's method
#       chapter 10 in numerical recipes in C
#       SCIPY: https://github.com/scipy/scipy/blob/v1.4.1/scipy/optimize/optimize.py
class Brent:
    #need to rethink design of __init__
    def __init__(self, func, args=(), tol=1.48e-8, maxiter=500,
                 full_output=0):
        self.func = func
        self.args = args
        self.tol = tol
        self.maxiter = maxiter
        self._mintol = 1.0e-11
        self._cg = 0.3819660
        self.xmin = None
        self.fval = None
        self.iter = 0
        self.funcalls = 0

    # need to rethink design of set_bracket (new options, etc)
    def set_bracket(self, brack=None):
        self.brack = brack

    # NOTE: this function was originally not part of the brent class in scipy.
    #       It is (as the whole brent class) a python implementation of
    #       code taken from Numerical Recipes. In particular this function
    #       is the function mnbrak (page 400)
    def bracket(self, func, xa=0.0, xb=1.0, args=(), grow_limit=110.0, maxiter=1000):
        """
        Bracket the minimum of the function.
        Given a function and distinct initial points, search in the
        downhill direction (as defined by the initital points) and return
        new points xa, xb, xc that bracket the minimum of the function
        f(xa) > f(xb) < f(xc). It doesn't always mean that obtained
        solution will satisfy xa<=x<=xb
        Parameters
        ----------
        func : callable f(x,*args)
            Objective function to minimize.
        xa, xb : float, optional
            Bracketing interval. Defaults `xa` to 0.0, and `xb` to 1.0.
        args : tuple, optional
            Additional arguments (if present), passed to `func`.
        grow_limit : float, optional
            Maximum grow limit.  Defaults to 110.0
        maxiter : int, optional
            Maximum number of iterations to perform. Defaults to 1000.
        Returns
        -------
        xa, xb, xc : float
            Bracket.
        fa, fb, fc : float
            Objective function values in bracket.
        funcalls : int
            Number of function evaluations made.
        """
        _gold = 1.618034  # golden ratio: (1.0+sqrt(5.0))/2.0
        _verysmall_num = 1e-21
        fa = func(*(xa,) + args)
        fb = func(*(xb,) + args)
        if (fa < fb):                      # Switch so fa > fb
            xa, xb = xb, xa
            fa, fb = fb, fa
        xc = xb + _gold * (xb - xa)
        fc = func(*((xc,) + args))
        funcalls = 3
        iter = 0
        while (fc < fb):
            tmp1 = (xb - xa) * (fb - fc)
            tmp2 = (xb - xc) * (fb - fa)
            val = tmp2 - tmp1
            if np.abs(val) < _verysmall_num:
                denom = 2.0 * _verysmall_num
            else:
                denom = 2.0 * val
            w = xb - ((xb - xc) * tmp2 - (xb - xa) * tmp1) / denom
            wlim = xb + grow_limit * (xc - xb)
            if iter > maxiter:
                raise RuntimeError("Too many iterations.")
            iter += 1
            if (w - xc) * (xb - w) > 0.0:
                fw = func(*((w,) + args))
                funcalls += 1
                if (fw < fc):
                    xa = xb
                    xb = w
                    fa = fb
                    fb = fw
                    return xa, xb, xc, fa, fb, fc, funcalls
                elif (fw > fb):
                    xc = w
                    fc = fw
                    return xa, xb, xc, fa, fb, fc, funcalls
                w = xc + _gold * (xc - xb)
                fw = func(*((w,) + args))
                funcalls += 1
            elif (w - wlim)*(wlim - xc) >= 0.0:
                w = wlim
                fw = func(*((w,) + args))
                funcalls += 1
            elif (w - wlim)*(xc - w) > 0.0:
                fw = func(*((w,) + args))
                funcalls += 1
                if (fw < fc):
                    xb = xc
                    xc = w
                    w = xc + _gold * (xc - xb)
                    fb = fc
                    fc = fw
                    fw = func(*((w,) + args))
                    funcalls += 1
            else:
                w = xc + _gold * (xc - xb)
                fw = func(*((w,) + args))
                funcalls += 1
            xa = xb
            xb = xc
            xc = w
            fa = fb
            fb = fc
            fc = fw
        return xa, xb, xc, fa, fb, fc, funcalls


    def get_bracket_info(self):
        #set up
        func = self.func
        args = self.args
        brack = self.brack
        ### BEGIN core bracket_info code ###
        ### carefully DOCUMENT any CHANGES in core ##
        if brack is None:
            xa, xb, xc, fa, fb, fc, funcalls = self.bracket(func, args=args)
        elif len(brack) == 2:
            xa, xb, xc, fa, fb, fc, funcalls = self.bracket(func, xa=brack[0],
                                                       xb=brack[1], args=args)
        elif len(brack) == 3:
            xa, xb, xc = brack
            if (xa > xc):  # swap so xa < xc can be assumed
                xc, xa = xa, xc
            if not ((xa < xb) and (xb < xc)):
                raise ValueError("Not a bracketing interval.")
            fa = func(*((xa,) + args))
            fb = func(*((xb,) + args))
            fc = func(*((xc,) + args))
            if not ((fb < fa) and (fb < fc)):
                raise ValueError("Not a bracketing interval.")
            funcalls = 3
        else:
            raise ValueError("Bracketing interval must be "
                             "length 2 or 3 sequence.")
        ### END core bracket_info code ###

        return xa, xb, xc, fa, fb, fc, funcalls

    def optimize(self):
        # set up for optimization
        func = self.func
        xa, xb, xc, fa, fb, fc, funcalls = self.get_bracket_info()
        _mintol = self._mintol
        _cg = self._cg
        #################################
        #BEGIN CORE ALGORITHM
        #################################
        x = w = v = xb
        fw = fv = fx = func(*((x,) + self.args))
        if (xa < xc):
            a = xa
            b = xc
        else:
            a = xc
            b = xa
        deltax = 0.0
        funcalls += 1
        iter = 0
        while (iter < self.maxiter):
            tol1 = self.tol * np.abs(x) + _mintol
            tol2 = 2.0 * tol1
            xmid = 0.5 * (a + b)
            # check for convergence
            if np.abs(x - xmid) < (tol2 - 0.5 * (b - a)):
                break
            # XXX In the first iteration, rat is only bound in the true case
            # of this conditional. This used to cause an UnboundLocalError
            # (gh-4140). It should be set before the if (but to what?).
            if (np.abs(deltax) <= tol1):
                if (x >= xmid):
                    deltax = a - x       # do a golden section step
                else:
                    deltax = b - x
                rat = _cg * deltax
            else:                              # do a parabolic step
                tmp1 = (x - w) * (fx - fv)
                tmp2 = (x - v) * (fx - fw)
                p = (x - v) * tmp2 - (x - w) * tmp1
                tmp2 = 2.0 * (tmp2 - tmp1)
                if (tmp2 > 0.0):
                    p = -p
                tmp2 = np.abs(tmp2)
                dx_temp = deltax
                deltax = rat
                # check parabolic fit
                if ((p > tmp2 * (a - x)) and (p < tmp2 * (b - x)) and
                        (np.abs(p) < np.abs(0.5 * tmp2 * dx_temp))):
                    rat = p * 1.0 / tmp2        # if parabolic step is useful.
                    u = x + rat
                    if ((u - a) < tol2 or (b - u) < tol2):
                        if xmid - x >= 0:
                            rat = tol1
                        else:
                            rat = -tol1
                else:
                    if (x >= xmid):
                        deltax = a - x # if it's not do a golden section step

                    else:
                        deltax = b - x
                    rat = _cg * deltax

            if (np.abs(rat) < tol1):            # update by at least tol1
                if rat >= 0:
                    u = x + tol1
                else:
                    u = x - tol1
            else:
                u = x + rat
            fu = func(*((u,) + self.args))      # calculate new output value
            funcalls += 1

            if (fu > fx):                 # if it's bigger than current
                if (u < x):
                    a = u
                else:
                    b = u
                if (fu <= fw) or (w == x):
                    v = w
                    w = u
                    fv = fw
                    fw = fu
                elif (fu <= fv) or (v == x) or (v == w):
                    v = u
                    fv = fu
            else:
                if (u >= x):
                    a = x
                else:
                    b = x
                v = w
                w = x
                x = u
                fv = fw
                fw = fx
                fx = fu

            iter += 1
        #################################
        #END CORE ALGORITHM
        #################################

        self.xmin = x
        self.fval = fx
        self.iter = iter
        self.funcalls = funcalls

    def get_result(self, full_output=False):
        if full_output:
            return self.xmin, self.fval, self.iter, self.funcalls
        else:
            return self.xminx
