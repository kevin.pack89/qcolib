#!/usr/bin/env python

# This code is part of qcolib.
#
# (C) Copyright Kevin Pack, Miralem Sinanovic 2020
#
# This code is licensed under the Apache License, Version 2.0. You may
# obtain a copy of this license in the LICENSE.txt file in the root directory
# of this source tree or at http://www.apache.org/licenses/LICENSE-2.0.
#
# Any modifications or derivative works of this code must retain this
# copyright notice, and modified files need to carry a notice indicating
# that they have been altered from the originals.

from typing import List

Vector = List[float]
Matrix = List[Vector]

"""
what this script does:
    - for each black box function (optimum at (1,1,1,...))
        -for each optimizer (each with x0 = 0)
            - 1 run without domain limits
            - 4 settings for the domain:
                - near origin (bb = [-2,2]^n)
                - close to bounds but not quite on bounds (bb = [-1.001,1.001]^n)
                - exactly on bounds (bb = [-1,1]^n)
                - outside bounds (bb = [-0.95,0.95]^n)


the results depending of each black box function get plotted in a different row
    the results for the respective optimizer get a new column each
        the 4 different settings go in the same plot


list of black box functions:
    - sphere
    - rosenbrock
    - rastrigin
TODO:expand by a few more?
"""


import numpy as np
from qcolib.optim import Optim

import pprint as pprint

import time

import matplotlib.pyplot as plt

#######################################################################################################################
# SPHERE FUNCTION
#######################################################################################################################

#TODO: add more functions.
#Note: all functions are rescaled by some factor to ensure that function values are in ~[0,1]

def f_sphere(x_in: Vector) -> float:
    """

    Parameters
    ----------
        x_in : Vector

    Returns
    -------
    float

    """
    x = np.copy(x_in)
    for i in range(0, np.shape(x)[0]):
        x[i] = x_in[i] - 1

    f = 0;
    for i in range(0, np.shape(x)[0]):
        f += x[i]**2

    f *= 0.01
    return f


def f_rosenbrock(x_in: Vector) -> float:
    """

    Parameters
    ----------
        x_in : Vector

    Returns
    -------
    float

    """
    x = np.copy(x_in)
    for i in range(0, np.shape(x)[0]):
        x[i] = x_in[i]

    f = 0
    for i in range(0, np.shape(x)[0]-1):
        f += 100*(x[i+1]-x[i]**2)**2 + (1-x[i])**2

    f *= 0.0001
    return f


def f_rastrigin(x_in: Vector) -> float:
    """

    Parameters
    ----------
        x_in : Vector

    Returns
    -------
    float

    """
    x = np.copy(x_in)
    for i in range(0, np.shape(x)[0]):
        x[i] = x_in[i]-1

    A = 10
    f = A*np.shape(x)[0]
    for i in range(0, np.shape(x)[0]):
        f += x[i]**2 - A*np.cos(2*np.pi*x[i])

    f *= 0.01
    return f


functions = [f_sphere, f_rosenbrock, f_rastrigin]
functions_name = ['f_sphere', "f_rosenbrock", "f_rastrigin"]


#######################################################################################################################
# Configurations for different algos
#######################################################################################################################

#TODO: Config for each algo

dim = 10
x0 = np.zeros(dim)
budget = 15000

#######################################
# CMAES

#sigma = 1.2
#C = np.eye(dim)

hparams_CMAES = {
    "x0" : np.zeros(dim),
#    "mean" : np.zeros(dim),
#    "C" : C,
    "popsize" : 10,
#    "sigma" : sigma
}

gparams_CMAES = {
    "name" : "CMAES",
    "dim" : dim,
    "budget" : budget,
    "hparams" : hparams_CMAES
}


#######################################
# Nelder Mead

hparams_NelderMead = {
#    "x0" : x0,      # Starting positions
#    "alpha" : 1,    # reflection coefficient
#    "gamma" : 2,    # expansion coefficient
#    "rho" : 0.5,    # contraction coefficient
#    "sigma" : 0.5   # shrink coefficient
}

gparams_NelderMead = {
    "name" : "NelderMead",
    "dim" : dim,
    "budget" : budget,
    "hparams" : hparams_NelderMead
}


#######################################
# OnePlusOne

hparams_OnePlusOne = {
    "x0" : x0,
#    "popsize" : 1
#    "sigma" : 1.5
}

gparams_OnePlusOne = {
    "name" : "OnePlusOne",
    "dim" : dim,
    "budget" : budget,
    "hparams" : hparams_OnePlusOne
}



#######################################

gparameters = [gparams_CMAES, gparams_NelderMead, gparams_OnePlusOne]



#######################################################################################################################
# function that does the stuff
#######################################################################################################################

def do_algo(func: callable, func_name: str, gparams: dict, colour: str, ax: object):
    """

    Parameters
    ----------
        func : callable
        func_name : str
        gparams : dict
        colour : str
        ax : object

    """
    opt = Optim(
        gparams = gparams
    )

    while not opt.is_converged():
            x = opt.ask()
            f_x = func(x)
            opt.tell(x, f_x)

    hist = opt.arc.hist

    x_val = np.arange(0,len(hist))
    y_val = np.zeros(len(hist))
    for i in range(0, len(hist)):
        y_val[i] = hist[i]['f_x']


    plt.plot(x_val, y_val, color=colour)
    plt.xscale('log')
    plt.yscale('log')
    plt.xlabel('# function calls')
    plt.ylabel('function value')
    ax.title.set_text(gparams["name"] + ' on ' + func_name)



#######################################################################################################################
# Main code
#######################################################################################################################

colours = ['blue', 'red', 'purple', 'green', 'black']


def domains(k: int, dim: int) -> Matrix:
    """

    Parameters
    ----------
        k : int
        dim : int

    Returns
    -------
    Matrix

    """

    retval = []
    if k==0:
        retval = []
    elif k==1:
        retval = np.zeros((dim,2))
        for i in range(0, dim):
            retval[i][0] = -2
            retval[i][1] = 2
    elif k==2:
        retval = np.zeros((dim,2))
        for i in range(0, dim):
            retval[i][0] = -1.0001
            retval[i][1] = 1.0001
    elif k==3:
        retval = np.zeros((dim,2))
        for i in range(0, dim):
            retval[i][0] = -1
            retval[i][1] = 1
    elif k==4:
        retval = np.zeros((dim,2))
        for i in range(0, dim):
            retval[i][0] = -0.99
            retval[i][1] = 0.99

    return retval



fig = plt.figure()
for i in range(0, len(functions)):
    for j in range(0, len(gparameters)):
        ax = fig.add_subplot(len(functions), len(gparameters), i*len(gparameters)+j+1)
        print(f"{gparameters[j]['name']} on {functions_name[i]}")
        for k in range(0, 5):
            gparameters[j]["domain"] = domains(k, gparameters[j]["dim"])
            do_algo(functions[i], functions_name[i], gparameters[j], colours[k], ax)
fig.tight_layout()
plt.show()

exit(0)
