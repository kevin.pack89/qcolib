# -*- coding: utf-8 -*-

# This code is part of qcolib.
#
# (C) Copyright Kevin Pack, Miralem Sinanovic 2020
#
# This code is licensed under the Apache License, Version 2.0. You may
# obtain a copy of this license in the LICENSE.txt file in the root directory
# of this source tree or at http://www.apache.org/licenses/LICENSE-2.0.
#
# Any modifications or derivative works of this code must retain this
# copyright notice, and modified files need to carry a notice indicating
# that they have been altered from the originals.

from typing import Union
from typing import List
from typing import Tuple

Vector = List[float]
Matrix = List[Vector]

import numpy as np
import threading
#from threading import Thread
#from threading import Event
#import mutex

from qcolib.utils import Convergence
from qcolib.libraries.transform.parameter import ParameterTransform as T
from qcolib.libraries.transform.parameter import ParameterInverseTransform as T_inv
from qcolib.libraries.transform.function import FunctionTransform as F

import sys
import queue


#TODO: rename AlgoBase -> OptimAlgoBase
class AlgoBase:
    """
    This is the base class for all optimizer. It unifies common attributes and the behaviour of each possible optimization algorithm.
    It also implements some of the basic implications from working on a quantum system like parameter transformation and function adaptation.

    Attributes
    ----------
        budget : int
            Maximum number of function evaluations before the algorithm stops
        event :
        atc :
        gparams : dict
            dictionary of non algorithm specific parameters 
        algo : callable
            function in which the algorithm is implemented in each child class
        endthread : bool
        thread : Threading
            the thread structure in which the algorithm is run
        output : Queue
            fifo queue used for the ask interface
        domain : Matrix
            vector of domain bounds [a,b] for each coordinate of the optimization space
        input : Queue
            fifo queue used for the tell interface
        g :
            helper function
        rng : 
            random number generator supplied to each algorithm (unused if deterministic algorithm)
        mutex :
        converged : bool
        x0 : Vector
            starting point of the optimization (each algorithm will try to start in an area around x0). zero vector by default
        arc :
        pc_scale : float
            scale of the primary cell in the optimizer space (if the space is bounded) (NOT WORKING YET)
        T : 
            Transformation between optimizer parameter space and physical parameter space
        F : 
            function in optimizer space that is actually being optimized
        hparams : dict
            dictionary containing all the algorithm specific parameters
        special_convergence : 
        dim : int
            dimension of the optimizing space

    """

    subclasses = []
    def __init_subclass__(cls, **kwargs):
        super().__init_subclass__(**kwargs)
        cls.subclasses.append(cls)

    def __init__(
        self,
        gparams: dict = {}
    ) -> None:
        """

        Parameters
        ----------
            gparams : dict
                dictionary containing all general parameters for the algorithm. MUST include the dimension of the problem (dim).

        Raises
        ------
            Exception : 
                Raises exception if the dimension of the problem is not set
        """
        self.gparams = gparams

        # GENERAL PARAMETERS THAT ARE USED BY ALL ALGORITHMS
        if "dim" in self.gparams:
            self.dim = self.gparams["dim"]
        else:
            # TODO: IF starting points provided obtain dim from them
            raise Exception("Dimension of problem not specified!")

        if "budget" in self.gparams:
            self.budget = self.gparams["budget"]
        else:
            print("Budget not specified. Fallback to default value: 1000.")
            self.budget = 1000

        if "domain" in self.gparams:
            self.domain = self.gparams["domain"]
        else:
            print("Domain not specified. Fallback to unlimited parameter range.")
            self.domain = []

        if "x0" in self.gparams:
            #TODO: raise exception if dimensions of x0 and dim dont fit
            self.x0 = T_inv(self.gparams["x0"], self.domain)
        else:
            self.x0 = np.zeros(self.dim)
            if(np.shape(self.domain) != np.shape([])):
                for i in range(0, len(x0)):
                    self.x0[i] = 0.5*(self.domain[i][1]-self.domain[i][0])

        self.arc = self.gparams["arc"]   # Archive of all points visited during optimization
        self.atc = self.gparams["atc"]
        self.hparams = self.gparams["hparams"]

        self.input = queue.Queue()
        self.output = queue.Queue()

        self.mutex = threading.Lock()
        self.converged = False
        self.pc_scale = 1.# primary cell size

        if "rng" in self.gparams:
            self.rng = self.gparams["rng"]
        else:
            sg = np.random.SeedSequence(123)
            self.rng = np.random.Generator(np.random.PCG64(sg.spawn(1)[0]))


    def start_thread(self) -> None:
        """
        This is called by the wrapper class to start the optimization algorithm
        """
        self.event = threading.Event()
        self.thread = threading.Thread(target=self.algo, args = (), daemon=True)
        self.thread.start()

    def f(self, x: Union[Vector, Matrix] ) -> Union[float, Vector]:
        """
        This function is used internaly by each algorithm, it pushes new x values to be evaluated to the
        ask-tell interface. It also handles any internal transformations between optimizer space and
        physical parameter space as well as conversions of the function values.

        This function can take either a single vector x or a vector of rowvectors x. 

        Parameters
        ----------
            x : Union[Vector, Matrix]
                the requested function value or values in optimization space

        Returns
        -------
        Union[float, Vector]
            either a single function value or a vector of function values, depending on the input
        """

        # Miralem: this transforms any other type of vector into a proper rowvector if I understand correctly
        # the reshape has to happen here unless one wants to rewrite all the code for powell - which I(kevin) don't ...
        reshape_flag = False
        if (np.shape(x) == (self.dim,)):
            reshape_flag = True
            x = np.asarray([x])


        if (np.shape(x) == (self.dim,)):
            return self.f_single(x)
        else: 
            return self.f_multi(x)



    def f_single(self, x: Vector) -> float:
        """
        This is the implementation of f for a single point

        Parameters
        ----------
            x : Vector
                a single vector in optimization space

        Returns
        -------
        float

        Raises
        ------
            Exception : raised if the tell doesnt match the ask
            Convergence : raised if the algorithm has reached the convergence criteria
        """
        Tx = T(x, self.domain)       # Transform parameters
        self.output.put(Tx)  # append to ask queue

        xy = self.input.get(block = True)
        self.check_convergence(xy)
        if(xy[0] != Tx):
            raise Exception("[ERROR] Value provided to opt.tell() does not fit value provided by opt.ask()")

        if self.converged:
            raise Convergence

        f_x = 0
        if reshape_flag == False:
            f_x = xy[1] # returns f_x
        else:
            if (np.shape(xy[1]) != (1,)):
                raise Exception("[ERROR] Too many function values returned! Aborting")
            f_x = xy[1][0] # take the first element out of the f_x array

        return F(f_x, x)



    def f_multi(self, x: Matrix) -> Vector:
        """

        Parameters
        ----------
            x : Matrix

        Returns
        -------
        Vector

        Raises
        ------
            Exception : raised if the tell doesnt match the ask
            Convergence : raised if the algorithm has reached the convergence criteria
        """
        # TODO: this function needs to handle the case that the number of function values requested is bigger than the budget
        #       currently it does not do so properly? check to be sure.
        size = np.shape(x)[0]
        F_x = np.zeros(size)

        for i in range(0,size):
            Tx = T(x[i], self.domain)
            self.output.put(Tx)

        for i in range(0, size):
            xy = self.input.get(block = True)
            self.check_convergence(xy)
            #TODO find index j s.t. Tx == xy[0] , for now we just assume fifo outer loops dont scramble the Tx
            j = i
            #TODO the following doesnt work because its apparently elemntwise... wtf python? I just want to compare objects.
            #if(np.not_equal(xy[0], self.T(x[j]))):
            #    raise Exception("[ERROR] Value provided to opt.tell() does not fit value provided by opt.ask()")

            if self.converged:
                raise Convergence

            F_x[j] = F(xy[1], x[j])

        return F_x



    def ask(self) -> Vector:
        """
        returns the next element in the output queue from the algorithm

        Returns
        -------
        Vector
            next point at which the function shall be evaluated
        """
        x = self.output.get(block = True, timeout = 0.5)
        return x # block = true timeout = 3 sec


    def tell(self, x: Vector, f_x: float) -> None:
        """
        adds an element to the input queue for the algorithm

        Parameters
        ----------
            x : Vector
                the point in physical space
            f_x : float
                the associated function value
        """
        self.input.put([x, f_x])

        # wait till convergence has been checked
        # this prevents a race condition with the outer while loop in the main code
        self.event.wait()


    #TODO I dont think the type here is correct for xy. xy is a vectors/value pair in a list
    def check_convergence(self, xy: Vector) -> None:
        """

        Parameters
        ----------
            xy : Vector
                
        """
        #TODO: rework the convergence system
        #TODO: if amax(x)>0.9999 => minimum near border => issue warning to inform user that area is probably not big enough
        self.mutex.acquire()
        x = xy[0]
        f_x = xy[1]
        conv1 = False
        conv2 = False

        # check if the child class has a special convergence requirement
        if hasattr(self, "special_convergence"):
            conv2 = self.special_convergence(x, f_x)
        else:
            if self.atc.cntr >= self.budget:
                conv1 = True
            if f_x < 1e-15:
                conv1 = True
                print("convergence reached by fidelity goal 1e-15")

        conv = conv1 or conv2

        # send signal to notify that self.converged got updated
        self.converged = conv
        self.event.set()
        self.mutex.release()


    def is_converged(self) -> bool:
        """

        Returns
        -------
            bool

        """
        self.mutex.acquire()
        ret = self.converged
        # if self.atc != 0:
        self.event.clear()
        self.mutex.release()

        return ret

    def check_hparam_shape(self, name: str, shape: Tuple[int]) -> bool:
        """
        checks if a given hparam has the correct shape

        Parameters
        ----------
            name : str
                string containing the name of the hyper parameter
            shape : tuple
                shape required for this hyperparameter

        Returns
        -------
        bool
            whether the hparam with name has the right shape
            

        Raises
        ------
            Exception : 
            np :
            str : str
            shape : tuple
            hparams : dict
        """
        tmp = False
        if name in self.hparams:
            if np.shape(self.hparams[name]) == shape:
                tmp = True
            else:
                raise Exception("Tried to set " + name + " but dimension are not matching! shape of " + name + ":" + str(np.shape(self.hparams[name])) +
                                " shape required: " + str(shape))
        return tmp


    def algo(self) -> None:
        """
        This function will be overloaded for the different optimization algorithms

        Raises
        ------
            NotImplementedError : 
                raised in case someone wants to run the base algo
        """
        # XXX implement this for each algo
        raise NotImplementedError



    def __del__(self) -> None:
        """
        Destructor for the algorithm object. Needs to signal to the thread that it should terminate.
        """
        self.endthread = True
        #self.thread.join()
