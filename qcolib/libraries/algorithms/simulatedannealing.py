# -*- coding: utf-8 -*-
import numpy as np

from qcolib.utils import Convergence
from qcolib.libraries.algorithms.algo_base import AlgoBase



class SimulatedAnnealing(AlgoBase):
    """
        Simulated-Annealing-Algorithm
    """

    def __init__(self, gparams={}):
        super().__init__(gparams=gparams)

        if self.check_hparam_shape("trials", ()):
            self.trials = self.hparams["trials"]
        else:
            self.trials = self.budget  # amount of trial vectors

        if self.check_hparam_shape("schedule", ()):
            self.schedule = self.hparams["schedule"]
        else:
            self.schedule = 'norm' # the chosen annealing schedule



    def newTemp(self, temp):
        if self.schedule == 'Annealing1':
            temp *= self.T_change
        elif self.schedule == 'norm':
            self.T_change = (self.n + 1) / self.budget
            temp *= self.T_change
        return temp



    def algo(self):

        x_try = self.x0
        x_current = np.asanyarray(x_try)
        f_current = self.f(x_try)
        self.n = 0

        if self.schedule == 'Annealing1':
            # Initiate Temperature-sequence
            p_init = 0.7  # taken from an article
            p_end = 0.001
            T_init = -1.0 / np.log(p_init)
            T_end = -1.0 / np.log(p_end)
            self.T_change = (T_end / T_init) ** (1.0 / (self.budget - 1.0))
            # number of accepted trials so far
            trial_number = 1.0
            # Initiate Temperature
            temp = T_init
            delta_avg = 0.0
        elif self.schedule == 'norm':
            self.T_change = 1/self.budget
            temp = 0.005   #starting temperature? classic value?


        try:
            while True:
                for m in range(self.trials):

                    # Generation of trial vectors
                    for j in range(self.dim):
                        x_try[j] = x_current[j] + 0.1 * np.random.uniform(-1, 1)
                        x_try[j] = max(min(x_try[j], 1), -1)
                    delta = abs(self.f(x_try) - f_current)

                    if self.n == 0 and m == 0:
                        delta_avg = delta

                    # Metropolis criterion
                    if delta < 0:
                        p = 0.0
                        if self.schedule == 'Annealing1':
                            p = np.exp(-1 * delta / (delta_avg * temp))
                        elif self.schedule == 'norm':
                            p = np.exp(-delta / temp)

                        # decide whether to keep the worse trial-vector or not
                        # TODO: Change uniform for Boltzmann
                        if np.random.uniform() < p:
                            keep = True
                        else:
                            keep = False
                    else:
                        keep = True
                    if keep:
                        x_current = x_try
                        f_current = self.f(x_current)
                        if self.schedule == 'Annealing1':
                            trial_number += 1
                            delta_avg = (delta_avg * (trial_number - 1.0) + delta) / trial_number

                # set up next iteration
                temp = self.newTemp(temp)
                self.n += 1
        except Convergence:
            pass
