# -*- coding: utf-8 -*-

# This code is part of qcolib.
#
# (C) Copyright Kevin Pack, Miralem Sinanovic 2020
#
# This code is licensed under the Apache License, Version 2.0. You may
# obtain a copy of this license in the LICENSE.txt file in the root directory
# of this source tree or at http://www.apache.org/licenses/LICENSE-2.0.
#
# Any modifications or derivative works of this code must retain this
# copyright notice, and modified files need to carry a notice indicating
# that they have been altered from the originals.

from typing import List

Vector = List[float]
Matrix = List[Vector]

import numpy as np

from qcolib.utils import Convergence
from qcolib.libraries.algorithms.algo_base import AlgoBase


class OnePlusLambda(AlgoBase):
    """
        1+lambda-ES algorithm
    """
    def __init__(
        self,
        gparams: dict = {}
    ) -> None:
        super().__init__(
            gparams = gparams
        )

        if self.check_hparam_shape("popsize", ()):
            self.popsize = self.hparams["popsize"]
        else:
            self.popsize = self.dim

        if self.check_hparam_shape("sigma", ()):
            self.sigma = self.hparams["sigma"]
        else:
            self.sigma  = 0.5

        self.x = self.x0


    def sample_new_x(self, dim: int, mean: Vector, sigma: float, popsize: int) -> Matrix:
        """
        Creates popsize points around mean with variation sigma according to the standard normal distribution
        Parameters
        ----------
            dim : dimension of the space
            mean : mean of distribution
            sigma : variation of distribution
            popsize : number of new points

        Returns
        -------
        Matrix
            array of new sample points

        """
        x_new  = np.zeros([popsize, dim])

        for i in range(0, popsize):
            Nd = self.rng.standard_normal(dim)
            x_new[i] = mean + sigma * Nd
        return x_new


    def algo(self) -> None:
        """
        1+lambda-ES algorithm implementation
        """
        self.f_x = self.f(self.x)

        try:
            while True:
                x_new = self.sample_new_x(self.dim, self.x, self.sigma, self.popsize)
                f_x_new = self.f(x_new)

                f_x_new_best = self.f_x
                x_new_best = self.x
                if(self.popsize > 1):
                    for i in range(self.popsize):
                        if i == 0:
                            f_x_new_best = f_x_new[0]
                            x_new_best = x_new[0]
                        if f_x_new[i] < f_x_new_best:
                            f_x_new_best = f_x_new[i]
                            x_new_best = x_new[i]
                else:
                    f_x_new_best = f_x_new
                    x_new_best = x_new

                if (f_x_new_best <= self.f_x):
                    self.x = x_new_best
                    self.f_x = f_x_new_best
                    self.sigma = self.sigma * 2.0
                else:
                    self.sigma = self.sigma * 2**(-0.25)

        except Convergence:
            pass
