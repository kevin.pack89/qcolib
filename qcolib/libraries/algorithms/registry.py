# -*- coding: utf-8 -*-

# This code is part of qcolib.
#
# (C) Copyright Kevin Pack, Miralem Sinanovic 2020
#
# This code is licensed under the Apache License, Version 2.0. You may
# obtain a copy of this license in the LICENSE.txt file in the root directory
# of this source tree or at http://www.apache.org/licenses/LICENSE-2.0.
#
# Any modifications or derivative works of this code must retain this
# copyright notice, and modified files need to carry a notice indicating
# that they have been altered from the originals.


import os
import pathlib
fs = [f for f in os.listdir(pathlib.Path(__file__).parent.absolute()) if (f.endswith('.py') and f!='registry.py' and f!='cmaes_mod.py')]

for f in fs:
    string = f'from qcolib.libraries.algorithms.{f[:-3]} import *'
    exec(string)

ALGORITHMS = dict()
def algo_reg(obj: object):
    """
    Decorator for making registry of functions

    Parameters
    ----------
        obj : object

    """
    name = getattr(obj, "__name__", obj.__class__.__name__)
    ALGORITHMS[name] = obj

for i in AlgoBase.subclasses:
    algo_reg(i)
