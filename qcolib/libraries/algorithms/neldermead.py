# -*- coding: utf-8 -*-

# This code is part of qcolib.
#
# (C) Copyright Kevin Pack, Miralem Sinanovic 2020
#
# This code is licensed under the Apache License, Version 2.0. You may
# obtain a copy of this license in the LICENSE.txt file in the root directory
# of this source tree or at http://www.apache.org/licenses/LICENSE-2.0.
#
# Any modifications or derivative works of this code must retain this
# copyright notice, and modified files need to carry a notice indicating
# that they have been altered from the originals.

from typing import List

Vector = List[float]
Matrix = List[Vector]

import numpy as np
from qcolib.utils import Convergence
from qcolib.libraries.algorithms.algo_base import AlgoBase
from qcolib.libraries.transform.parameter import ParameterInverseTransform as T_inv


class NelderMead(AlgoBase):
    """
    Nelder Mead algorithm
    """
    def __init__(
        self,
        gparams: dict = {}
    ):
        """

        Parameters
        ----------
            gparams : dict
                dictionary with the non algorithm specific parameters

        """
        super().__init__(
            gparams = gparams
        )

        self.popsize = self.dim + 1

        # coefficients of the algorithm
        # init standard values
        if self.check_hparam_shape("alpha", ()):
            self.alpha = self.hparams["alpha"]
        else:
            self.alpha  = 1     # reflection coefficient

        if self.check_hparam_shape("gamma", ()):
            self.gamma = self.hparams["gamma"]
        else:
            self.gamma  = 2     # expansion coefficient

        if self.check_hparam_shape("rho", ()):
            self.rho = self.hparams["rho"]
        else:
            self.rho    = 0.5   # contraction coefficient

        if self.check_hparam_shape("sigma", ()):
            self.sigma = self.hparams["sigma"]
        else:
            self.sigma  = 0.5   # shrink coefficient

        if self.check_hparam_shape("x_initial", (self.popsize, self.dim)):
            self.x = np.zeros([self.popsize, self.dim])
            tmp = self.hparams["x_initial"]
            for i in range (0, np.shape(tmp)[0]):
                self.x[i] = T_inv(tmp[i], self.domain)

        else:
            self.x = self.init_population(self.popsize, self.dim)


    def init_population(self, popsize: int, dim: int) -> Matrix:
        """

        Parameters
        ----------
            popsize : int
            dim : int

        Returns
        -------
        Matrix

        """
        #TODO: create regular simplex around x0
        x  = np.zeros([popsize, dim])
        rn = np.zeros([popsize, dim])

        x[0] = self.x0
        for i in range(1, popsize):
            x[i] = self.x0
            x[i][i-1] += 0.3
        return x


    def sort_solutions(self, x: Matrix, f_x: Vector) -> Matrix:
        """

        Parameters
        ----------
            x : Matrix
            f_x : Vector

        Returns
        -------
        Matrix

        """
        arg = np.argsort(f_x)
        return x[arg[::]]


    def calc_centre(self, x: Matrix, dim: int) -> Vector:
        """

        Parameters
        ----------
            x : Matrix
            dim : int

        Returns
        -------
        Vector
            
        """
        # calculate centre of all points except(!) x_(n - 1)
        # dim = popsize -1 = n - 1
        cntr = np.zeros(dim)
        for i in range(0, dim):
            cntr = cntr + x[i]
        return (cntr / dim)


    def calc_reflection(self, cntr: Vector, wp: Vector) -> Vector:
        """

        Parameters
        ----------
            cntr : Vector
            wp : Vector

        Returns
        -------
        Vector
            
        """
        # calculate reflected point: x_r = cntr + alpha * (cntr + wp)
        #   for alpha = 1:
        #   self.r = 2*self.m - self.w
        return cntr + self.alpha * (cntr - wp)


    def calc_expansion(self, cntr: Vector, wp: Vector) -> Vector:
        """

        Parameters
        ----------
            cntr : Vector
            wp : Vector

        Returns
        -------
        Vector
            
        """
        # calculate expanded point x_e = cntr + gamma * (x_r - cntr)
        # gamma = 2
        # TODO: mismatch between matlab code and WIKIPEDIA!!! matlab: x_e = cntr + gamma * (wp - cntr)
        # MATLAB:
        return cntr + self.gamma * (cntr - wp)
        # WIKIPEDIA:
        #return cntr + 2 * (x_r - cntr)


    def shrink_simplex(self, x: Matrix, popsize: int) -> Matrix:
        """

        Parameters
        ----------
            x : Matrix
            popsize : int

        Returns
        -------
        Matrix

        """
        for i in range(1, popsize):
            x[i] = x[0] + self.sigma * (x[i] - x[0])
        return x


    def algo(self) -> None:
        """
        Nelder-Mead Algorithm
        """

        self.f_x = np.zeros([self.popsize])
        for i in range(0, self.popsize):
            self.f_x[i] = self.f(self.x[i])

        try:
            while True:
                # 1. SORTING
                x_sort = self.sort_solutions(self.x, self.f_x)
                f_x_sort = self.f_x[np.argsort(self.f_x)]

                self.x = x_sort
                self.f_x = f_x_sort

                # 2. CALCULATE CENTRE
                cntr = self.calc_centre(self.x, self.dim)

                # 3. REFLECTION
                wp = self.x[-1] # worst point <=> x_sort[popsize - 1]
                #f_wp = self.f_x[-1]

                x_r = self.calc_reflection(cntr, wp)


                f_x_r = self.f(x_r) # evaluate goal function at x_r


                if (self.f_x[0] <= f_x_r) and (f_x_r <= self.f_x[self.dim]):
                    self.f_x[-1] = f_x_r
                    self.x[-1] = x_r
                    continue    # GOTO: 1. ORDER

                elif (f_x_r < self.f_x[0]):
                    # 4. EXPANSION
                    x_e = self.calc_expansion(cntr, wp)
                    f_x_e = self.f(x_e) # evaluate goal function at x_e
                    if (f_x_e < f_x_r):
                        self.f_x[-1] = f_x_e
                        self.x[-1] = x_e
                        continue    # GOTO: 1. Order
                    else:
                        self.f_x[-1] = f_x_r
                        self.x[-1] = x_r
                        continue    # GOTO: 1. Order
                else:
                    # 5. CONTRACTION
                    if (f_x_r < self.f_x[-1]):
                        # NOTE: THIS CASE IS UNIQUE TO THE MATLAB CODE AND IS NOT PART OF THE WIKIPEDIA IMPLEMENTATION
                        x_c = cntr + self.rho * (x_r - cntr)
                        f_x_c = self.f(x_c)  # evaluate goal function at x_c
                        if (f_x_c < f_x_r):
                            self.f_x[-1] = f_x_c
                            self.x[-1] = x_c
                            continue    # GOTO: 1. Order
                    if (f_x_r >= self.f_x[-1]):
                        # calculate contracted point: x_c = cntr + rho * (wp - cntr). rho = 0.5
                        x_c = cntr + self.rho * (wp - cntr)
                        f_x_c = self.f(x_c)  # evaluate goal function at x_c
                        if (f_x_c < self.f_x[-1]):
                            self.f_x[-1] = f_x_c
                            self.x[-1] = x_c
                            continue    # GOTO: 1. Order
                    # 6. SHRINK
                    # if none of the above cases apply shrink the simplex
                    self.x = self.shrink_simplex(self.x, self.popsize)

                    # 0. EVALUATE GOAL FUNCTION VALUES
                    self.f_x = np.zeros([self.popsize])
                    self.f_x[1:self.popsize-1] = self.f(self.x[1:self.popsize-1])

        except Convergence:
            pass

    def __del__(self) -> None:
            return
