# -*- coding: utf-8 -*-

# This code is part of qcolib.
#
# (C) Copyright Kevin Pack, Miralem Sinanovic 2020
#
# This code is licensed under the Apache License, Version 2.0. You may
# obtain a copy of this license in the LICENSE.txt file in the root directory
# of this source tree or at http://www.apache.org/licenses/LICENSE-2.0.
#
# Any modifications or derivative works of this code must retain this
# copyright notice, and modified files need to carry a notice indicating
# that they have been altered from the originals.

from typing import List

Vector = List[float]
Matrix = List[Vector]

import sys
import numpy as np
from qcolib.libraries.algorithms.algo_base import AlgoBase
from qcolib.utils import Brent
from qcolib.utils import Convergence

class Powell(AlgoBase):
    """ Powell algorithm"""
    def __init__(
        self,
        gparams: dict = {}
    ):
        """

        Parameters
        ----------
            gparams : Dictionary with non algorithm specific parameters

        """
        super().__init__(
                gparams = gparams
        )

        self.x = self.x0

        if self.check_hparam_shape("v0", (self.dim, self.dim)):
            self.direc = self.hparams["v0"]


        # accuracy
        #self.epsilon = 0.5
        self.direc = np.eye(self.dim, dtype=float)

        self.xtol = 1e-4 # Relative error in solution `xopt` acceptable for convergence.
        self.ftol = 1e-4 # Relative error in ``fun(xopt)`` acceptable for convergence.


    def linesearch_powell(self, func: callable, p: Vector, xi: Vector, tol:float =1e-3) -> (float, Vector, Vector):
        """Line-search algorithm using fminbound.
        Find the minimium of the function ``func(x0+ alpha*direc)``.
        Does a 1D search along a given axis given by p and xi of the function func and stops when tolerance tol is reached

        Parameters
        ----------
            func : callable
            p : Vector
                affinity of the linear 1D search space
            xi : Vector
                search direction
            tol : float
                tolerance for the 1D serach

        Returns
        -------
        float, Vector, Vector
            function value, new argmin and search direction

        """

        brack=None
        args=()
        #xtol=1.48e-8
        maxiter=500

        def myfunc(alpha):
            return func(p + alpha*xi)

        brent = Brent(func=myfunc, args=args, tol=tol, full_output=True, maxiter=maxiter)
        brent.set_bracket(brack)
        brent.optimize()

        x, fval, nit, nfev = brent.get_result(full_output=True)

        alpha_min = x
        fret = fval
        iter = nit
        num = nfev
        xi = alpha_min*xi
        return np.squeeze(fret), p + xi, xi


    # https://github.com/scipy/scipy/blob/v1.4.1/scipy/optimize/optimize.py
    def algo(self) -> None:

        self.f_x = self.f(self.x)

        x = self.x[0]
        fval = self.f_x[0]
        x1 = self.x[0]
        iter = 0
        ilist  = list(range(self.dim))

        try:
            while True:
                fx = fval
                bigind = 0
                delta = 0.0

                for i in ilist:
                    direc1 = self.direc[i]
                    fx2 = fval
                    fval, x, direc1 = self.linesearch_powell(self.f, x, direc1, tol=(self.xtol * 100))


                    if (fx2 - fval) > delta:
                        delta = fx2 - fval
                        bigind = i
                iter += 1


                # Construct the extrapolated point
                direc1 = x - x1
                x2 = 2 * x - x1
                x1 = x.copy()
                fx2 = np.squeeze(self.f(x2))

                if (fx > fx2):
                    t = 2.0 * (fx + fx2 - 2.0 * fval)
                    temp = (fx - fval - delta)
                    t *= temp * temp
                    temp = fx - fx2
                    t -= delta * temp * temp
                    if t < 0.0:
                        fval, x, direc1 = self.linesearch_powell(self.f, x, direc1, tol=self.xtol*100)
                        self.direc[bigind] = self.direc[-1]
                        self.direc[-1] = direc1

        except Convergence:
            pass
