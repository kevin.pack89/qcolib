# -- coding: utf-8 --

# This code is part of qcolib.
#
# (C) Copyright Kevin Pack, Miralem Sinanovic 2020
#
# This code is licensed under the Apache License, Version 2.0. You may
# obtain a copy of this license in the LICENSE.txt file in the root directory
# of this source tree or at http://www.apache.org/licenses/LICENSE-2.0.
#
# Any modifications or derivative works of this code must retain this
# copyright notice, and modified files need to carry a notice indicating
# that they have been altered from the originals.

from typing import List

Vector = List[float]
Matrix = List[Vector]

import numpy as np
import scipy
from qcolib.utils import Convergence
from qcolib.libraries.transform.parameter import ParameterInverseTransform as T_inv

from qcolib.libraries.algorithms.algo_base import AlgoBase


class CMAES(AlgoBase):
    def __init__(
        self,
        gparams: dict = {}
    ):
        """

        Parameters
        ----------
            gparams : dict
                dicitonary with the settings for the algorithm
        """
        super().__init__(
                gparams = gparams
        )
        # User defined input parameters (need to be edited)
        self.hparams = gparams["hparams"]


        if self.check_hparam_shape("C", (self.dim, self.dim)):
            self.C = self.hparams["C"]
            self.C = np.triu(self.C) + np.transpose(np.triu(self.C, 1)) # enforce symmetry

            #TODO throw an appropriate error if C is not positive definite
            D, B = np.linalg.eig(self.C) #  eigen decomposition, B==normalized eigenvectors
            D = np.sqrt(np.diag(D)) # D is a vector of standard deviations now
            rs = np.matmul(np.linalg.inv(D), np.transpose(B))
            self.invsqrtC = np.matmul(B, rs)
            self.cholC = np.matmul(B, D)

        else:
            self.C = np.eye(self.dim, self.dim)
            self.cholC = np.copy(self.C)
            self.invsqrtC = np.copy(self.C)

        if self.check_hparam_shape("sigma", ()):
            self.sigma = self.hparams["sigma"]
        else:
            self.sigma  = 0.3

        if self.check_hparam_shape("popsize", ()):
            self.popsize = self.hparams["popsize"]
        else:
            self.popsize = int(4 + np.floor(3 * np.log(self.dim)))

        if self.check_hparam_shape("x_initial", (self.popsize, self.dim)):
            self.x = np.zeros([self.popsize, self.dim])
            tmp = self.hparams["x_initial"]
            for i in range (0, np.shape(tmp)[0]):
                self.x[i] = T_inv(tmp[i], self.domain)
            self.mean = np.sum(np.asarray(self.x), axis = 0) / np.shape(np.asarray(self.x))[0]
        else:
            if self.check_hparam_shape("mean",(self.dim,)):
                self.mean = T_inv(self.hparams["mean"], self.domain)
            else:
                self.mean = self.x0
            self.x = self.sample_new_x(self.popsize, self.dim, self.cholC, self.mean, self.sigma)


        # internal counters
        self.eigeneval = 0
        self.first_iter = True

        # initialize state variables
        # TODO: wikipedia example code calls mu = floor(mu) after calculating weights. difference? if so why?
        self.mu = int(np.floor(self.popsize / 2))     # number of parents/points for recombination
        self.weights = self.init_weights(self.mu)
        self.mueff = self.calc_mueff(self.mu, self.weights)

        # adaption parameters (good)/ constants used by the updated functions
        self.cc = (4 + self.mueff / self.dim) / (self.dim + 4 + 2 * self.mueff / self.dim)
        self.cs = (self.mueff + 2) / (self.dim + self.mueff + 5)
        self.c1 = 2 / ((self.dim + 1.3) * (self.dim + 1.3) + self.mueff)
        self.cmu = min(1 - self.c1, 2 * (self.mueff - 2 + 1 / self.mueff) / ((self.dim + 2) * (self.dim + 2) + self.mueff))
        self.damps = 1 + 2 * max(0, np.sqrt((self.mueff - 1) / (self.dim + 1)) - 1) + self.cs
        # expectation of ||N(0,I)|| == norm(randn(N,1))
        self.chiN = np.sqrt(self.dim) * (1 - 1 / (4 * self.dim) + 1 / (21 * self.dim * self.dim))



    def init_weights(self, mu: int) -> Vector:
        """

        Parameters
        ----------
            mu : int

        Returns
        -------
        Vector
            
        """
        weights = np.zeros(mu)
        for i in range(0, mu):
            weights[i] = np.log(mu + 0.5) - np.log(i + 1)
        return weights / np.sum(weights)


    def calc_mueff(self, mu: int, weights: Vector) -> float:
        """

        Parameters
        ----------
            mu : int
            weights : Vector

        Returns
        -------
        float

        """
        s = np.sum(weights * weights)
        return np.sum(weights) * np.sum(weights) / s


    def update_mean(self, mu: int, x_sort: Matrix, weights: Vector) -> Vector:
        """

        Parameters
        ----------
            mu : int
            x_sort : Matrix
            weights : Vector

        Returns
        -------
        Vector

        """
        # compute weighted mean
        tmp = np.zeros(np.shape(x_sort[0]))
        for i in range(0, mu):
            tmp += weights[i] * x_sort[i]
        return tmp


    def update_ps(self, cs: float, mueff: float, invsqrtC: Matrix, ps: Vector, sigma: float, mean_diff: Vector) -> Vector:
        """

        Parameters
        ----------
            cs : float
            mueff : float
            invsqrtC : Matrix
            ps : Vector
            sigma : float
            mean_diff : Vector

        Returns
        -------
        Vector

        """
        tmp = np.sqrt(mueff) * invsqrtC.dot(mean_diff / sigma)
        return (1 - cs) * ps + np.sqrt(1 - (1 - cs) * (1 - cs)) * tmp


    def calc_hsig(self, ps: Vector, cs: float, atc_cntr: int, popsize: int, chiN: float, dim: int) -> bool:
        """

        Parameters
        ----------
            ps : Vector
            cs : float
            atc_cntr : int
            popsize : int
            chiN : float
            dim : int

        Returns
        -------
        bool

        """
        return np.linalg.norm(ps) / np.sqrt(1 - np.power((1 - cs),(2 * atc_cntr / popsize))) / chiN < 1.4 + 2 / (dim + 1)


    def update_pc(self, cc: float, pc: Vector, hsig: bool, mueff: float, mean_diff: Vector, sigma: float) -> Vector:
        """

        Parameters
        ----------
            cc : float
            pc : Vector
            hsig : bool
            mueff : float
            mean_diff : Vector
            sigma : float

        Returns
        -------
        Vector
            

        """
        return (1 - cc) * pc + hsig * np.sqrt(1 - (1 - cc) * (1 - cc)) * np.sqrt(mueff) * mean_diff / sigma


    def update_C(self, x_sort: Matrix, mu: int, sigma: float, c1: float, cmu: float, C: Matrix, pc: Vector, weights: Vector, hsig: bool, cc: float, mean_old: Vector) -> Matrix:
        """

        Parameters
        ----------
            x_sort : Matrix
            mu : int
            sigma : float
            c1 : float
            cmu : float
            C : Matrix
            pc : Vector
            weights : Vector
            hsig : bool
            cc :float
            mean_old : Vector

        Returns
        -------
        Matrix
        
        """
        # TODO: error??? according to wiki (1-self.c1-self.cmu + CS) * self.C: + CS MISSING???
        t1 = (1 - c1 - cmu) * C

        # TODO: where does the additional term come from? not mentioned in wiki
        pc_trans = pc[np.newaxis]
        pc_col = np.transpose(pc_trans)
        t2 = c1 * (np.matmul(pc_col, pc_trans) + (1 - hsig) * cc * (2 - cc) * C)

        tmp = (x_sort[0:mu] - np.tile(mean_old, (mu, 1))) / sigma
        # ALTERNATIVE CODE
        # tmp2 = np.zeros(np.shape(C))
        # for i in range(mu):
        #     vec_trans = tmp[i]
        #     vec_trans = vec_trans[np.newaxis]
        #     vec_col = np.transpose(vec_trans)
        #     tmp2 += weights[i] * np.matmul(vec_col, vec_trans)
        # t32 = cmu * tmp2
        t3 = cmu * np.matmul(np.transpose(tmp), np.matmul(np.diag(weights), tmp))

        return t1 + t2 + t3


    def invsqrt(self, M: Matrix) -> Matrix:
        """
        Custom Function to quickly calculate the inverse square root of a matrix. (does not require to do an eigendecomposition)
        This function should be faster than doing an eigendecomposition and can do decomposition in some cases where np.eig fails

        Args:
            M: Matrix
                a positive definite matrix

        Returns: Matrix
            an inverse square root of M

        """
        cc = np.linalg.cholesky(M)
        cc = np.transpose(cc)
        U = cc
        rel = 0
        while True:
            Ut = np.transpose(U)
            inv = np.linalg.inv(Ut)
            tmp = 0.5*(U + inv)

            rel = np.linalg.norm(tmp-U)
            U = tmp
            if rel<1e-10:
                break

        res = np.matmul(np.transpose(U), cc)
        res = np.linalg.inv(res)
        return res


    def update_sigma(self, sigma: float, cs: float, damps: float, ps: Vector, chiN: float) -> float:
        """

        Parameters
        ----------
            sigma : float
            cs : float
            damps : float
            ps : Vector
            chiN : float

        Returns
        -------
        float

        """
        # Update step size
        return sigma * np.exp((cs / damps) * (np.linalg.norm(ps) / chiN - 1))


    def sort_solutions(self, x: Matrix, f_x: Vector) -> Matrix:
        """

        Parameters
        ----------
            x : Matrix
            f_x : Vector

        Returns
        -------
        Matrix

        """
        arg = np.argsort(f_x)   # returns indices of the sorted entries in f_x
        # uncomment next line to maximize instead of minimize
        # arg = np.flip(arg)
        return x[arg[::]]   # sort solutions by the indices provided through argsort


    def sample_new_x(self, popsize: int, dim: int, cholC: Matrix, mean: Vector, sigma: float) -> Matrix:
        """

        Parameters
        ----------
            popsize : int
            dim : int
            cholC : Matrix
            mean : Vector
            sigma : float

        Returns
        -------
        Matrix

        """
        x  = np.zeros([popsize, dim])
        rn = np.zeros([popsize, dim])

        for i in range(0, popsize):
            rn[i] = self.rng.standard_normal(dim)


        for i in range(0, popsize):
            x[i] = mean + sigma * cholC.dot(rn[i])

        return x


    def algo(self) -> None:
        """
        CMA-ES algorithm implementation
        """
        # dynamic variables
        self.pc = np.zeros(self.dim)
        self.ps = np.zeros(self.dim)

        try:
            while True:

                self.f_x = self.f(self.x)

                # Sort by fitness
                x_sort = self.sort_solutions(self.x, self.f_x)

                # updating mean / move mean to better solutions
                mean_old = self.mean
                self.mean = self.update_mean(self.mu, x_sort, self.weights)
                mean_diff = self.mean - mean_old     # elementwise difference for mean and mean_old being numpy arrays

                # update isotropic evolution path
                self.ps = self.update_ps(self.cs, self.mueff, self.invsqrtC, self.ps, self.sigma, mean_diff)

                # needed in update_pc and update_C
                hsig =  self.calc_hsig(self.ps, self.cs, self.atc.cntr, self.popsize, self.chiN, self.dim)

                # update anisotropic evolution path
                self.pc = self.update_pc(self.cc, self.pc, hsig, self.mueff, mean_diff, self.sigma)


                # update covariance matrix
                self.C = self.update_C(x_sort, self.mu, self.sigma, self.c1, self.cmu, self.C, self.pc,
                                       self.weights, hsig, self.cc, mean_old)

                # update sigma/step-size
                self.sigma = self.update_sigma(self.sigma, self.cs, self.damps, self.ps, self.chiN)

                # Calculate invsqrtC and cholC
                if self.atc.cntr - self.eigeneval > self.popsize/(self.c1+self.cmu)/self.dim/10:
                    self.eigeneval = self.atc.cntr

                    self.C = np.triu(self.C) + np.transpose(np.triu(self.C, 1)) # enforce symmetry

                    self.invsqrtC = self.invsqrt(self.C)
                    self.cholC = np.linalg.cholesky(self.C)
                    #Miralem: TODO: catch linalg error and calculate using my method instead
                    # I reverted the code since this doesnt work properly for some bigger popsizes
                    """
                    D, B = np.linalg.eig(self.C) #  eigen decomposition, B==normalized eigenvectors
                    D = np.sqrt(np.diag(D)) # D is a vector of standard deviations now
                    rs = np.matmul(np.linalg.inv(D), np.transpose(B))
                    self.invsqrtC = np.matmul(B, rs)
                    self.cholC = np.matmul(B, D)
                    """


                self.x = self.sample_new_x(self.popsize, self.dim, self.cholC, self.mean, self.sigma)


        except Convergence:
            pass

    def __del__(self) -> None:
        return
