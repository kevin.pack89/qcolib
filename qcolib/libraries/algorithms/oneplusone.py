# -*- coding: utf-8 -*-

# This code is part of qcolib.
#
# (C) Copyright Kevin Pack, Miralem Sinanovic 2020
#
# This code is licensed under the Apache License, Version 2.0. You may
# obtain a copy of this license in the LICENSE.txt file in the root directory
# of this source tree or at http://www.apache.org/licenses/LICENSE-2.0.
#
# Any modifications or derivative works of this code must retain this
# copyright notice, and modified files need to carry a notice indicating
# that they have been altered from the originals.

from typing import List

Vector = List[float]

import numpy as np

from qcolib.utils import Convergence
from qcolib.libraries.algorithms.algo_base import AlgoBase


class OnePlusOne(AlgoBase):
    """
        1+1-ES algorithm
    """
    def __init__(
        self,
        gparams: dict = {}
    ):
        super().__init__(
            gparams = gparams
        )

        # initial distribuation spread
        if self.check_hparam_shape("sigma", ()):
            self.sigma = self.hparams["sigma"]
        else:
            self.sigma  = 0.5

        self.x = self.x0


    def sample_new_x(self, dim: int, mean: Vector, sigma: float) -> Vector:
        """
        Samples a new point from the standard normal distribution with mean 'mean' and variance 'sigma'

        Parameters
        ----------
            dim : dimension of the space
            mean : mean of the distribution
            sigma : variance of the distribution (uniform in all directions)

        Returns
        -------
        Vector
            a randomly sampled vector around mean with variance sigma

        """
        Nd = self.rng.standard_normal(dim)
        x_new = mean + sigma * Nd
        return x_new



    def algo(self) -> None:
        """
        1+1-ES algorithm implementation
        """
        self.f_x = np.zeros(1)
        self.f_x = self.f(self.x)

        try:
            while True:
                x_new = self.sample_new_x(self.dim, self.x, self.sigma)
                f_x_new = self.f(x_new)

                if (f_x_new <= self.f_x):
                    self.x = x_new
                    self.f_x = f_x_new
                    self.sigma = self.sigma * 2.0   # do NOT change these values! read REMARK!
                else:
                    self.sigma = self.sigma * 2**(-0.25)  # do NOT change these values! read REMARK!

        except Convergence:
            pass


# REMARK:
# I do not know why but the choice of 2.0 and 0.84 as factors for the expansion and shrinking of
# sigma seem like magical working numbers. Other values seem not to work.
