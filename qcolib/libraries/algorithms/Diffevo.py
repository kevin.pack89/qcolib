# -*- coding: utf-8 -*-
import numpy as np

from qcolib.utils import Convergence
from qcolib.libraries.algorithms.algo_base import AlgoBase



class DifferentialEvolution(AlgoBase):
    """
        Differential-Evolution Algorithm
    """

    def __init__(self, gparams = {}):

        super().__init__( gparams = gparams )

        if self.check_hparam_shape("population", ()):
            self.population = self.hparams["population"]
        else:
            self.population  =  self.dim    # Population of vectors

        if self.check_hparam_shape("crossover", ()):
            self.crossover = self.hparams["crossover"]
        else:
            self.crossover  =  0.7          # crossover probability

        if self.check_hparam_shape("weight", ()):
            self.weight = self.hparams["weight"]
        else:
            self.weight  =  0.9            # differential weight

    def algo(self):
        Mx = np.random.randn(self.population, self.dim)

        try:
            while True:
                for i in range(0,self.population):

                    [a, b, c] = np.random.choice(np.delete(np.arange(0,self.population),i),3,replace=False)

                    # choose Index to be changed in any case
                    j = np.random.randint(0,self.dim)

                    # generate trial Vector
                    t = np.zeros(self.dim)

                    # next up is Crossover Phase
                    for k in np.arange(0, self.dim):
                        r = np.random.random()
                        if r < self.crossover or k == j:
                            t[k] = Mx[a,k]+ self.weight * (Mx[b,k] - Mx[c,k])
                        else:
                            t[k] = Mx[i,k]
                        j = (j+1) % self.dim
                    # selection Phase
                    if self.f(t) <= self.f(Mx[i]):
                        Mx[i] = t
        except Convergence:
            pass
