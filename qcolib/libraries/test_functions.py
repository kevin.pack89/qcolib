"""
Benchmark functions to test algorithms.
All functions are rescaled by some factor to ensure that function values are in ~[0,1]
"""

import numpy as np

REGISTRY_OF_FUNCS = dict()


def func_reg_deco(func):
    """
    Decorator for making registry of functions
    """
    REGISTRY_OF_FUNCS[str(func.__name__)] = func
    return func


@func_reg_deco
def sphere(x_in: np.ndarray) -> float:
    """basic test bed for functions

    Parameters
    ----------
    x_in : np.ndarray
        parameter set

    Returns
    -------
    float
        transformed function value
    """
    x = np.copy(x_in)
    for i in range(0, np.shape(x)[0]):
        x[i] = x_in[i] - 1

    f = 0.0
    for i in range(0, np.shape(x)[0]):
        f += x[i] ** 2

    f *= 0.01
    return f


@func_reg_deco
def rastrigin(x_in: np.ndarray) -> float:
    """Classical Multimodal Function

    Parameters
    ----------
    x_in : np.ndarray
        parameter set

    Returns
    -------
    float
        transformed function value
    """
    x = np.copy(x_in)
    for i in range(0, np.shape(x)[0]):
        x[i] = x_in[i] - 1.0

    A = 10
    f = A * np.shape(x)[0]
    for i in range(0, np.shape(x)[0]):
        f += x[i] ** 2 - A * np.cos(2 * np.pi * x[i])

    f *= 0.01
    return f


@func_reg_deco
def rosenbrock(x_in: np.ndarray) -> float:
    """Non Convex Banana Function

    Parameters
    ----------
    x_in : np.ndarray
        parameter set

    Returns
    -------
    float
        transformed function value
    """
    x = np.copy(x_in)
    for i in range(0, np.shape(x)[0]):
        x[i] = x_in[i]

    f = 0.0
    for i in range(0, np.shape(x)[0] - 1):
        f += 100 * (x[i + 1] - x[i] ** 2) ** 2 + (1 - x[i]) ** 2

    f *= 0.0001
    return f
