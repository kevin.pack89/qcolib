# -*- coding: utf-8 -*-

# This code is part of qcolib.
#
# (C) Copyright Kevin Pack, Miralem Sinanovic 2020
#
# This code is licensed under the Apache License, Version 2.0. You may
# obtain a copy of this license in the LICENSE.txt file in the root directory
# of this source tree or at http://www.apache.org/licenses/LICENSE-2.0.
#
# Any modifications or derivative works of this code must retain this
# copyright notice, and modified files need to carry a notice indicating
# that they have been altered from the originals.


from typing import Union
from typing import List

Vector = List[float]
Matrix = List[Vector]

import numpy as np



def f_helper(x: float) -> float:
    """
    A helper function.
    Its a monotonic function with a support of [0, inf).

    Parameters
    ----------
        x : float

    Returns
    -------
    float

    """
    if x<=0:
        return 0
    else:
        return np.exp(-1/x)



def h(x: Vector, optim_scale: float = 1) -> float:
    """
    This function has a compact support [-b,b], is bounded from above by 1
    and is constant in [-a,a], where it takes on its maximal value of 1.
    
    Parameters
    ----------
        x : Vector

    Returns
    -------
    float

    """
    a = optim_scale*3
    b = optim_scale*4
    retval = 1
    for i in range(0, len(x)):
        retval *= 1 - f_helper(abs(x[i])-a)/(f_helper(abs(x[i])-a) + f_helper(b-abs(x[i])))
    return retval


def g(x: Vector, optim_scale: float = 1) -> float:
    """
    this function is 0 inside [-optim_scale, optim_scale]^n and continuously goes 
    over into a linear function outside of this box

    Parameters
    ----------
        x : Vector

    Returns
    -------
    float

    """
    retval = 0
    for i in range(0, len(x)):
        if abs(x[i]) > optim_scale:
            retval += (abs(x[i])-optim_scale)*np.exp(-1/(optim_scale-abs(x[i]))**2)

    return retval


def FunctionTransform(f_x: float, x: Vector, optim_scale: float = 1) -> float:
    """
    The function in optimizer space that is actually being optimized

    Parameters
    ----------
        f_x : float
            function value at x in physical space
        x : Vector
            location in optimizer space

    Returns
    -------
    float

    """
    F_x = h(x, optim_scale)*f_x + g(x, optim_scale)

    return F_x
