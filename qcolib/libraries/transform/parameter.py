# -*- coding: utf-8 -*-

# This code is part of qcolib.
#
# (C) Copyright Kevin Pack, Miralem Sinanovic 2020
#
# This code is licensed under the Apache License, Version 2.0. You may
# obtain a copy of this license in the LICENSE.txt file in the root directory
# of this source tree or at http://www.apache.org/licenses/LICENSE-2.0.
#
# Any modifications or derivative works of this code must retain this
# copyright notice, and modified files need to carry a notice indicating
# that they have been altered from the originals.

from typing import List
from typing import Tuple

Vector = List[float]
Matrix = List[Vector]

import numpy as np


def ParameterTransform(x: Vector, domain: Matrix, optim_scale: float = 1) -> Vector:
    """
    Transformation between optimizer parameter space and physical parameter space

    Parameters
    ----------
        x : Vector
            input vector in optimization space

    Returns
    -------
    Vector
        output vector in physical space
    """
    Tx = np.copy(x)
    if(np.shape(domain) != np.shape([])):
        for i in range(0, len(x)):
            if len(domain[i]) == 2:
                a = domain[i][0]
                b = domain[i][1]

                tmp = np.sin(np.pi/2 * x[i]/optim_scale)   # collapse Real numbers to [-1,1]
                tmp = 0.5*(tmp + 1)                        # rescale [-1,1] to [0,1]
                Tx[i] = a + tmp*(b-a)                      # rescale [0,1] to [a,b]
        return Tx
    else:
        return Tx
 

def ParameterInverseTransform(Tx: Vector, domain: Matrix, optim_scale: float = 1) -> Vector:
    """
    The reverse of T

    Parameters
    ----------
        Tx : Vector
            input vector in physical space

    Returns
    -------
    Vector
        output vector in optimization space
    """
    x = np.copy(Tx)
    if(np.shape(domain) != np.shape([])):
        for i in range(0, len(Tx)):
            if len(domain[i]) == 2:
                a = domain[i][0]
                b = domain[i][1]

                tmp = (Tx[i]-a)/(b-a)                            # rescale [a,b] to [0,1]
                tmp = 2*(tmp-0.5)                                # rescale [0,1] to [-1,1]
                x[i] = optim_scale *2/np.pi*np.arcsin(tmp)     # undo sine transformation
        return x
    else:
        return x
