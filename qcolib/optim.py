# -*- coding: utf-8 -*-

# This code is part of qcolib.
#
# (C) Copyright Kevin Pack, Miralem Sinanovic 2020
#
# This code is licensed under the Apache License, Version 2.0. You may
# obtain a copy of this license in the LICENSE.txt file in the root directory
# of this source tree or at http://www.apache.org/licenses/LICENSE-2.0.
#
# Any modifications or derivative works of this code must retain this
# copyright notice, and modified files need to carry a notice indicating
# that they have been altered from the originals.

from typing import List

Vector = List[float]
Matrix = List[Vector]

import os
import pprint
import pickle
from qcolib.libraries.algorithms.registry import ALGORITHMS as algorithms
from qcolib.utils import Archive
from qcolib.utils import ATC

class Optim:
    """
    wrapper around algo base, adding the  

    Attributes
    ----------
        gparams : dict
            Dictionary with non algorithm specific parameters
        arc : 
            archive of all visited points during an optimization
        set_algo :
        atc : int
            ask-tell counter
        algo : 
        name : str
            the name of the algorithm that is to be used

    """
    def __init__(
        self,
        gparams = {},
    ) -> None:

        self.gparams = gparams

        if "name" in self.gparams:
            self.name = self.gparams["name"]
        else:
            raise Exception("No algorithm specified for use!")

        self.arc = Archive()
        self.gparams["arc"] = self.arc

        self.atc = ATC()
        self.gparams["atc"] = self.atc

        self.algo = self.set_algo(self.name, self.gparams)
        self.algo.start_thread()


    def add_to_history(self, x: Vector, f_x: float) -> None:
        """
        Adds a vector with its corresponding function value to the history of visited points

        Parameters
        ----------
            x : Vector
                the point
            f_x : float
                its function value

        """
        self.arc.add_to_history(self.atc.cntr, x, f_x)


    def print_history(self, hist = {}, hist_range = []):
        """

        Parameters
        ----------
            hist :
            hist_range :

        """
        self.arc.print_history(hist, hist_range)


    def export_pickle(self, hist = {}, hist_range = []):
        """

        Parameters
        ----------
            hist :
            hist_range :

        """
        self.arc.export_pickle(hist, hist_range)


    def import_pickle(self, hist_range = []):
        """

        Parameters
        ----------
            hist_range :

        """
        self.arc.import_pickle(hist_range)


    def export_setup(self, dir, name, ftype):
        """

        Parameters
        ----------
            dir :
            name :
            ftype :

        """
        self.arc.export_setup(dir, name, ftype)


    def import_setup(self, dir, name, ftype):
        """

        Parameters
        ----------
            dir :
            name :
            ftype :

        """
        self.arc.import_setup(dir, name, ftype)


    def export_history(self, hist = {}, hist_range = []):
        """

        Parameters
        ----------
            hist :
            hist_range :

        """
        self.arc.export_history(hist, hist_range)


    def import_history(self, hist_range = []):
        """

        Parameters
        ----------
            hist_range :

        """
        self.arc.import_history(hist_range)


    def print_algo_list(self):
        """
        Prints a list of the available algorithms
        """
        print(list(algorithms.keys()))


    def set_algo(self, name, gparams):
        """

        Parameters
        ----------
            name : str
            gparams : dict

        Returns
        -------
            
        """
        return algorithms[self.name](self.gparams)


    def is_converged(self):
        """
        asks the algorithm if it is converged

        Returns
        -------
        bool
            the result
        """
        return self.algo.is_converged()


    def ask(self):
        """
        asks the optimizer for a vector to evaluate the function on

        Returns
        -------
        Vector
            the vector
        """
        return self.algo.ask()


    def tell(self, x: Vector, f_x: float) -> None:
        """
        tells the optimizer the function value f_x at vector x

        Parameters
        ----------
            x : Vector
                the parameter vector in physical space
            f_x : float
                the corresponding function value
        """
        self.add_to_history(x, f_x)
        #self.arc.add_to_history(self.atc.cntr, x, f_x)
        self.algo.tell(x, f_x)
        self.atc.cntr += 1
