.. qcolib documentation master file, created by
   sphinx-quickstart on Thu Apr 15 19:49:31 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.
============================================================================


==================================
Welcome to qcolib's documentation!
==================================

The ``qcolib`` package provides an easy ask-and-tell interface for using different gradient-free optimizers. It is designed for simplicity and customisability.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   cmaes
   powell
   qcolib


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
