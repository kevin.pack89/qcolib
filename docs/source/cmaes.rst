CMA-ES
=======

The covariance matrix adaptation evolution strategy, also called $( \mu/\mu_W,\lambda )$-CMA-ES algorithm
(short: CMA-ES algorithm) is a quite complex evolutionary algorithm which aims to be competitive to classic
algorithms on noiseless function while being usable in wide set of situations.
