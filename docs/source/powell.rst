Powell
=======

The Powell method in its basic form can be viewed as a gradient free minimization algorithm.
It requires repeated line search minimizations, which may be carried out using univariate gradient free,
or gradient based procedures. It was introduced by M.J.D. Powell.
