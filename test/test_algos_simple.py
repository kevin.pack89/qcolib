"""Module for testing cmaes algorithm
"""

from typing import Callable
import numpy as np
from qcolib.libraries.test_functions import REGISTRY_OF_FUNCS as FUNCTIONS
from qcolib.optim import Optim
import pytest

from qcolib.libraries.algorithms.registry import ALGORITHMS

def do_algo(func: Callable, gparams: dict) -> float:
    """Wrapper for running algorithms on test functions

    Parameters
    ----------
    func : Callable
        test function for running algorithm
    gparams : dict
        general parameters for the algorithm

    Returns
    -------
    float
        the final value after optimisation
    """
    opt = Optim(gparams=gparams)

    while not opt.is_converged():
        x = opt.ask()
        f_x = func(x)
        opt.tell(x, f_x)

    hist = opt.arc.hist

    y_val = np.zeros(len(hist))
    for i in range(0, len(hist)):
        y_val[i] = hist[i]["f_x"]

    return y_val[-1]


@pytest.mark.unit
@pytest.mark.parametrize(
    "algo", ALGORITHMS
)
def test_algo_dim_10(algo) -> None:

    # setup params
    hparams = {}
    dim = 10
    gparams = {
        "name": algo,
        "dim": dim,
        "budget": 15000,
        "x0": np.zeros(dim),
        "domain": [],
        "hparams": hparams
    }

    func = FUNCTIONS["sphere"]

    y_val = do_algo(func, gparams)
    assert y_val < 10**-10
