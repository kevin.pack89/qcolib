"""Module for testing the timeout in the ask interface
"""

import numpy as np
from qcolib.optim import Optim
import pytest
import queue

@pytest.mark.unit
def test_timeout() -> None:
    hparams = {}
    gparams = {
            "name": "OnePlusOne",
            "dim": 1,
            "budget": 100,
            "hparams": hparams
    }

    opt = Optim(gparams=gparams)

    a = 0
    try:
        x = opt.ask()
        x = opt.ask()
    except queue.Empty:
        a = 1

    assert a == 1
