"""
    Module for testing constraints using the cmaes algorithm on different test
    functions
"""

from typing import Callable
from typing import List
import numpy as np
from qcolib.libraries.test_functions import REGISTRY_OF_FUNCS as FUNCTIONS
from qcolib.libraries.algorithms.registry import ALGORITHMS
from qcolib.optim import Optim
import pytest


def do_algo(func: Callable, gparams: dict) -> float:
    """Wrapper for running algorithms on test functions

    Parameters
    ----------
    func : Callable
        test function for running algorithm
    gparams : dict
        general parameters for the algorithm

    Returns
    -------
    float
        the final value after optimisation
    """
    opt = Optim(gparams=gparams)

    while not opt.is_converged():
        x = opt.ask()
        f_x = func(x)
        opt.tell(x, f_x)

    hist = opt.arc.hist

    y_val = np.zeros(len(hist))
    for i in range(0, len(hist)):
        y_val[i] = hist[i]["f_x"]

    return y_val[-1]

@pytest.mark.unit
@pytest.mark.parametrize(
    "domain_1D, best_val", [([], 10**-9), ([-100, 100], 10**-9), ([-2,2], 10**-9), ([-1,1], 10**-9), ([-0.5, 0.5], 0.025+10**-9)]
)
def test_cmaes(domain_1D: List[float], best_val: float) -> None:
    """Test CMA-ES on Sphere function

    Parameters
    ----------
    dim : int
        number of parameter
    budget : int
        number of function evaluations
    popsize : int
        population size for evolution
    final_val : float
        final value after optimisation
    """
    dim  = 10
    domain = np.repeat([domain_1D], dim, axis=0)
    # setup params
    hparams_CMAES = {}

    gparams_CMAES = {
        "name": "CMAES",
        "dim": dim,
        "budget": 15000,
        "domain": domain,
        "x0": np.zeros(dim),
        "hparams": hparams_CMAES,
    }

    func = FUNCTIONS["sphere"]

    y_val = do_algo(func, gparams_CMAES)
    assert y_val < best_val

