"""Module for testing cmaes algorithm
"""

from typing import Callable
import numpy as np
from qcolib.libraries.test_functions import REGISTRY_OF_FUNCS as FUNCTIONS
from qcolib.optim import Optim
import pytest

from qcolib.libraries.algorithms.registry import ALGORITHMS

def do_algo(func: Callable, gparams: dict) -> float:
    """Wrapper for running algorithms on test functions

    Parameters
    ----------
    func : Callable
        test function for running algorithm
    gparams : dict
        general parameters for the algorithm

    Returns
    -------
    float
        the final value after optimisation
    """
    opt = Optim(gparams=gparams)

    while not opt.is_converged():
        x = opt.ask()
        f_x = func(x)
        opt.tell(x, f_x)

    hist = opt.arc.hist

    y_val = np.zeros(len(hist))
    for i in range(0, len(hist)):
        y_val[i] = hist[i]["f_x"]

    return y_val[-1]

@pytest.mark.unit
@pytest.mark.parametrize(
    "dim, budget, popsize, final_val", [(10, 15000, 10, 1e-10), (20, 30000, 10, 1e-10)]
)
def test_cma(dim: int, budget: int, popsize: int, final_val: float) -> None:
    """Test CMA-ES on Sphere function

    Parameters
    ----------
    dim : int
        number of parameter
    budget : int
        number of function evaluations
    popsize : int
        population size for evolution
    final_val : float
        final value after optimisation
    """

    # setup params
    hparams_CMAES = {"x0": np.zeros(dim), "popsize": popsize}

    gparams_CMAES = {
        "name": "CMAES",
        "dim": dim,
        "budget": budget,
        "hparams": hparams_CMAES
    }

    func = FUNCTIONS["sphere"]

    y_val = do_algo(func, gparams_CMAES)
    assert y_val < final_val

