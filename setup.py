from distutils.core import setup

setup(
    name='qcolib',
    version='0.1',
    packages=[
        'qcolib',
    ],
    long_description=open('README.md').read(),
    install_requires=[
        'numpy',
    ]
)
